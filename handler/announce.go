package handler

import (
	"gitlab.com/meeting-master/sdk/announce/model"
	"gitlab.com/meeting-master/sdk/announce/repository"
	"gitlab.com/meeting-master/sdk/announce/service"
	authRepo "gitlab.com/meeting-master/sdk/auth/repository"
	authorization "gitlab.com/meeting-master/sdk/auth/service"
	"gitlab.com/meeting-master/sdk/connector/pgsql"
	"gitlab.com/meeting-master/sdk/errors"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

type AnnounceHandler struct {
	service service.AnnounceService
}

func newAnnounceHandler(router *gin.Engine, db *pgsql.DB) *gin.Engine {
	repo := repository.NewAnnounceRepository(db)
	auth := authorization.NewAuthService(authRepo.NewAuthRepository(db))
	s := service.InitWithEngine(repo, auth, nil)

	handler := &AnnounceHandler{service: s}

	router.GET("/announces-to-validate", handler.AnnouncesToValidate)
	router.POST("/announce-validate", handler.Validate)

	return router
}

func (h *AnnounceHandler) AnnouncesToValidate(gc *gin.Context) {
	ctx, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}
	res, err := h.service.AnnouncesToValidate(ctx)
	if err != nil {
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, Response{
		Data: res,
	})
}

func (h *AnnounceHandler) Validate(gc *gin.Context) {
	var request model.ValidationRequest
	if er := gc.ShouldBindJSON(&request); er != nil {
		log.Println(er.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}

	ctx, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}

	 err := h.service.Validate(ctx, request)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, "")
}
