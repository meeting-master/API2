package handler

import (
	"gitlab.com/meeting-master/sdk/location/model"
	"log"
	"net/http"

	"gitlab.com/meeting-master/sdk/connector/pgsql"
	"gitlab.com/meeting-master/sdk/location/repository"
	"gitlab.com/meeting-master/sdk/location/service"

	"github.com/gin-gonic/gin"
)

type LocationHandler struct {
	service service.LocationService
}

func newLocationHandler(router *gin.Engine, db *pgsql.DB) *gin.Engine {
	repo := repository.NewLocationRepository(db)
	s := service.NewLocationService(repo)

	handler := &LocationHandler{service:s}

	routerPosition := router.Group("/location")
	{
		routerPosition.POST("/country/add", handler.NewCountry)
		routerPosition.POST("/town/add", handler.NewTown)
		routerPosition.POST("/district/add", handler.NewDistrict)
		routerPosition.GET("/countries", handler.Countries)
		routerPosition.GET("/towns", handler.Towns)
		routerPosition.GET("/districts", handler.Districts)
		routerPosition.GET("/locations-by-district", handler.LocationByDistrict)
		routerPosition.GET("/locations-by-town", handler.LocationByTown)
	}

	return router
}

func (h *LocationHandler) NewCountry(c *gin.Context) {
	var request model.CountryRequest
	if err := c.ShouldBindJSON(&request); err != nil {
		log.Println(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	countryId, err := h.service.UpsertCountry(request)
	if err != nil {
		log.Println(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, Response{ID:countryId})
}

func (h *LocationHandler) NewTown(c *gin.Context) {
	var request model.TownRequest
	if err := c.ShouldBindJSON(&request); err != nil {
		log.Println(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	id, err := h.service.UpsertTown(request)
	if err != nil {
		log.Println(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, Response{ID: id})
}

func (h *LocationHandler) NewDistrict(c *gin.Context) {
	var request model.DistrictRequest
	if err := c.ShouldBindJSON(&request); err != nil {
		log.Println(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	id, err := h.service.UpsertDistrict(request)
	if err != nil {
		log.Println(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, Response{ID: id})
}

func (h *LocationHandler) Countries(c *gin.Context) {
	countries, err := h.service.Countries()
	if err != nil {
		log.Println(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, Response{Data: countries})
}

func (h *LocationHandler) Towns(gc *gin.Context) {
	countryId := gc.Query("countryId")
	tw, err := h.service.Towns(countryId)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	gc.JSON(http.StatusOK, Response{Data: tw})
}

func (h *LocationHandler) Districts(gc *gin.Context) {
	townId := gc.Query("townId")
	tw, err := h.service.Districts(townId)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	gc.JSON(http.StatusOK, Response{Data: tw})
}

func (h *LocationHandler) LocationByDistrict(c *gin.Context) {
	localizations, err := h.service.LocationByDistrict()
	if err != nil {
		log.Println(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, Response{Data: localizations})
}

func (h *LocationHandler) LocationByTown(c *gin.Context) {
	localizations, err := h.service.LocationByTown()
	if err != nil {
		log.Println(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, Response{Data: localizations})
}
